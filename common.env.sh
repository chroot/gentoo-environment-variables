PAGER=most
EDITOR=emacs
BROWSER=w3m
WWW_HOME=searx.org
PATH=~/.local/bin:$PATH

typeset -x PAGER EDITOR BROWSER PATH
